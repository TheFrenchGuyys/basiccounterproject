using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterCode;

namespace BasicCounterTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Testdecrementation()
        {
            Counter counter = new Counter();
            Assert.AreEqual(0, counter.decrementation());
        }

        public void Testincrementation()
        {
            Counter counter = new Counter();
            Assert.AreEqual(1, counter.incrementation());
        }

        public void TestRAZ()
        {
            Counter counter = new Counter();
            Assert.AreEqual(0, counter.RAZ());
        }
    }
}
