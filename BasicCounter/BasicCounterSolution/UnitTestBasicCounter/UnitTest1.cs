using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestBasicCounter
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]

            public void Testdecrementation()
            {
                Assert.Equals(2, BasicCounter.decrementation(3));
            }

            public void Testincrementation()
            {
                Assert.Equals(26, BasicCounter.incrementation(25));
            }

            public void TestRAZ()
            {
                Assert.Equals(0, BasicCounter.RAZ(0));
            }

    }
}
