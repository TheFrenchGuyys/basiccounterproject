Imports BasicCounterCode

Public Class Form1
    Dim compteur As New Counter()

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Label2.Text = compteur.decrementation()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Label2.Text = compteur.incrementation()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Label2.Text = compteur.RAZ()
    End Sub

End Class
