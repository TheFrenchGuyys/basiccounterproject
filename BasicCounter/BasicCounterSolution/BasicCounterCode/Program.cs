using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BasicCounterCode
{
    public class Counter
    {
        public int compteur = 0;

        public void Compteur()
        {
            this.compteur = 0;
        }

        public int decrementation()
        {
            if (compteur == 0)
            {
                compteur = 0;
            }
            else
            {
                this.compteur = compteur - 1;
            }

            return compteur;
        }

        public int incrementation()
        {
            this.compteur = compteur + 1;
            return compteur;
        }

        public int RAZ()
        {
            this.compteur = 0;
            return compteur;
        }

        public void Setcompteur(int compteur)
        {
            this.compteur = 0;
        }

        public int Getcompteur()
        {
            return compteur;
        }

        static void Main(string[] args)
        {

        }

    }
}